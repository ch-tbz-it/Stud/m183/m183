# m183 - Applikationssicherheit implementieren
**Modulidentifikation:** https://www.modulbaukasten.ch/module/183

**Modulentwicklung ZH:** https://gitlab.com/modulentwicklungzh/cluster-api/m183

**Modulverantwortlicher:** Stefan Kemper (stefan.kemper@tbz.ch)

## Überblick
Folgende Themen werden in diesem Modul behandelt:
- Repetition Schutzziele
- Häufige Schwachstellen, wie diese ausgenutzt werden könnten und wie die Schwachstelle abgesichert bzw. gefixt wird.
- Auffinden bzw. Identifizieren von Schwachstellen im Code (manuelles Vorgehen wie auch der Einsatz von PenTesting-Tools)
- Authentifizierung und Autorisierung (und generell der Umgang mit Zugangsdaten innerhalb des Codes der Applikation), inkl. Sessionhandling
- Massnahmen im Design der Applikation, die die Entstehung von Sicherheitslücken verhindern
- Überwachung der Applikation im Betrieb (Logging, Monitoring & Alarmierung).

Folgende Themen können optional mit behandelt werden (abhängig von der Klasse und der Lehrperson):
- Organisatorische Massnahmen, um die IT-Sicherheit zu erhöhen (ISO 27001, IKT Minimalstandard) - mit Fokus auf die Applikationsentwicklung
- Risikomanagement mit Fokus auf die Applikationsentwicklung (Umgang mit gefundenen bzw. bekannten aber noch nicht gefixten Schwachstellen)
- CTF: Gamification in der IT-Security
- Honeypot: ein Weg, um produktiven Traffic vom Traffic von Angreifern zu separieren
- Applikationssicherheit in der Cloud: Vertiefung / Repetition der bereits in den Modulen 346 und 347 erarbeiteten Inhalte


## Abhängigkeiten zu anderen Modulen
Folgende Themen sind für die Applikationssicherheit relevant, wurden aber bereits in anderen Modulen behandelt:
- Kryptographie: [Modul 114](https://www.modulbaukasten.ch/module/114), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m114)
- Datenschutz (& Verschlüsselung): [Modul 231](https://www.modulbaukasten.ch/module/231), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m231)
- Sicherheit in der Cloud: [Modul 346](https://www.modulbaukasten.ch/module/346), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m346)
- Sicherheit bei Containern (containerized web apps): [Modul 347](https://www.modulbaukasten.ch/module/347), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m347)

Die dort behandelten Themen werden im Modul 183 nochmals repetiert und wenn nötig weiter vertieft.

Im [Modul 321](https://www.modulbaukasten.ch/module/321) wird das Thema Authentisierung und Autorisierung nochmals aufgegriffen - dort vor allem im Zusammenhang mit verteilten Systemen (Stichwort IAM). In diesem Modul wird deshalb alles zu IAM noch nicht behandelt.

## Lizenz
...
