Die Modulnote setzt sich aus folgenden Teilen zusammen:
- LB1: Auftrag OWASP & Demo
- LB2: Auftrag Penetrationtesting
- LB3: Prüfung - mündlich oder schriftlich

Die Lehrperson wird Sie über alle weiteren Details (inkl. Gewichtung der einzelnen Teile) zu den Leistungsbeurteilungen zur gegebenen Zeit informieren.