# Authentifizierung und Autorisierung

[TOC]

## Einleitung
Die Fachbegriffe Authentifizierung und Autorisierung werden als bekannt vorausgesetzt, weil diese im [Modul 231](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/03_Authentifizierung%20und%20Autorisierung.md) bereits behandelt wurden. Wenn Sie sich unsicher sind, lesen Sie dort bitte nochmals nach wie sich die Begriffe definieren.

Hier liegt der Fokus darauf, dass Sie verstehen, wie Sie in Ihrer Applikation die Authentifizierung und Autorisierung der Benutzer sicher umsetzen können.

## Authentifizierung
Um einen Benutzer zu authentifizieren (seine Identität zu prüfen) gibt es die folgenden möglichen Massnahmen (Liste nicht abschliessend):
- Eingabe von Benutzername und Passwort
- Einmalpasswort
- Security-Token oder Token (https://de.wikipedia.org/wiki/Security-Token)
- TAN, eingesetzt bei e-Banking (https://de.wikipedia.org/wiki/Transaktionsnummer)
- Überprüfung biometrischer Merkmale

Bei der Mehrfaktorauthentifizierung (kurz MFA) geht es darum, dass für das Login nicht nur etwas (z. B. Benutzernamen und Passwort), sondern mehrere Dinge (z. B. Benutzernamen und Passwort sowie Einmalpasswort) überprüft werden. Sind nur zwei "Faktoren" mit im Spiel wird auch von 2FA gesprochen. Bei drei unterschiedlichen Faktoren von 3FA.

Wichtig für eine gute Mehrfaktorauthentifizierung ist, dass die verschiedenen Faktoren unabhängig voneinander sind bzw. von anderer Art sind. Folgende unterschiedliche Faktoren gibt es:
- **Wissen:** Etwas das Sie wissen - wie beispielsweise die Benutzername/Passwort-Kombination.
- **Besitz:** Etwas das Sie besitzen - wie beispielsweise ihr Smartphone oder ihre E-Mailadresse. Auf die wird dann beispielsweise ein Einmalpasswort gesendet, welches Sie eingeben müssen.
- **Inhärenz:** Etwas das Sie sind - also biometrische Eigenschaften wie beispielsweise Ihr Fingerabdruck oder Ihr Gesicht (z. B. bei Gesichtserkennung).
- **Ort:** Ein Ort wo Sie sich zum Zeitpunkt der Authentifizierung befinden. Beispielsweise könnten Sie eine Authentifizierung nur an einem gewissen Standort erlauben (z. B. nur dann, wenn Sie sich innerhalb des Firmennetzwerks mit Ihrem Rechner angeschlossen haben).

[Quelle & Details](https://de.wikipedia.org/wiki/Multi-Faktor-Authentisierung)

## Autorisierung
Bei der Autorisierung geht es darum, dem Benutzer die nötigen Rechte zu gewähren damit dieser die Applikation bestimmungsgemäss verwenden kann. Er sollte dabei so wenig Rechte wie möglich, aber so viel Rechte wie nötig erhalten ([least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege)).

Um die Verwaltung der Berechtigungen zu vereinfachen, wird in der Regel mit Gruppen und Rollen gearbeitet, um die einzelnen Berechtigungen den Benutzern zuzuweisen.
* **Gruppen**: Benutzer, die die gleichen Aufgaben innerhalb einer Applikation erledigen müssen, werden in Gruppen zusammengefasst. Auf einem Netzlaufwerkshare können dies beispielsweise die Mitarbeiter der unterschiedlichen Abteilungen wie "Buchhaltung", "Personal", "Geschäftsleitung", "Verkauf", etc. sein. Ein Benutzer kann in vielen Fällen auch mehreren Gruppen zugewiesen werden.
* **Rollen**: Im Unterschied zu Gruppen ist bei Rollen der Fokus eher auf der Tätigkeit der Benutzer bzw. der Gruppen von Benutzern. Einem Benutzer bzw. einer Gruppe können mehrere Rollen zugewiesen werden. Üblicherweise kann eine Rolle auch mehrfach zugewiesen werden. Eine Rolle könnte beispielsweise das Auslesen von Personal-Stammdaten umfassen. Diese Rolle kann sowohl für die Geschäftsleitung (beispielsweise ein um ein Mailing an die Mitarbeiter zu versenden) wie auch für die Personal-Abteilung (für den Versand von Lohnabrechnungen) relevant sein. Eine Rolle "Einsicht in Lohndaten" könnte aber beipsielsweise nur den Benutzern der Personal-Abteilung zugewiesen werden, weil die Geschäftsleitung nicht unbedingt die Löhne der einzelnen Mitarbeiter kennen muss.

### Temporäre Berechtigungen
Abhängig von der aktuellen Tätigkeit können Berechtigungen auch temporär gewährt werden - in Abhängigkeit vom Status innerhalb eines Geschäftsprozesses oder auch in Abhängigkeit von der Zeit. Beispielsweise wäre vorstellbar, dass eine Applikation nur zu Geschäftszeiten gewisse Änderungen zulässt, diese aber ausserhalb der Geschäftszeiten nur noch den Administratoren vorbehalten bleibt.

Die Idee hinter diesen temporären Berechtigungen ist es, dem [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege) möglichst nahezukommen. Beispielsweise werden für die tägliche Arbeit am Rechner keine Admin-Privilegien benötigt. Wenn aber Software installiert werden muss, dann sind Admin-Privilegien nötig. Das System kann dann beispielsweise nochmals das Benutzerpasswort bzw. die Benutzerzustimmung verlangen, dass eine spezifische Aktion ausgeführt werden darf. Die erweiterten Admin-Rechte gelten dann nur für diese Aktion und werden dem Benutzer im Anschluss wieder entzogen.

Im Cloud-Umfeld existiert dieses Konzept ebenfalls. Bei AWS wird beispielsweise von Temporary Security Credentials (Details zu finden unter [AWS STS](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_temp.html)) gesprochen. Microsoft nennt dies Just-in-Time (JIT) Access (Details zu [JIT](https://learn.microsoft.com/de-de/azure/defender-for-cloud/just-in-time-access-overview) bzw. [PIM - Privileged Identity Management](https://learn.microsoft.com/en-us/entra/id-governance/privileged-identity-management/pim-configure) welches JIT Access beinhaltet). Trotz der sehr unterschiedlichen Bezeichnungen von AWS und Microsoft steckt die gleiche Grundidee dahinter, Zugriffe nur so lange zu gewähren wie unbedingt notwendig für die Erledigung der konkreten Aufgabe.
