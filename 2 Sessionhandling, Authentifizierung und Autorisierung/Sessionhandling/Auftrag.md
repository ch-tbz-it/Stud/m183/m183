# Sessionhandling Auftrag

[TOC]

## Lernziele
* Verstehen, wie Sessionhandling funktioniert
* Die sicherheitsrelevanten Aspekte beim Sessionhandling verstehen
* Die Auswirkungen verstehen / erleben, wenn Sessions durch einen Angreifer übernommen würden

## Rahmenbedingungen
Dieser Auftrag ist in Gruppen von 2-3 Personen zu lösen. Aufwand ca. 25-45 Minuten.

## Installation
Installieren Sie das Script (alle Files unter [AufgabeSource](./AufgabeSource)) auf einem PHP-fähigen Webserver auf welchen alle in Ihrer Gruppe Zugriff haben (z. B. ein XAMPP und Zugriff über die IP, die Sie via WLAN kriegen). Nur jemand in der Gruppe braucht diesen Schritt zu machen. Die anderen Gruppenmitglieder unterstützen.

## Auftrag
Öffnen Sie die installierte Webapplikation (Webseite) auf Ihrem Browser. Diese sollte ungefähr wie folgender Screenshot aussehen:
![ScreenshotSessionApp](../Ressourcen/ScreenshotSessionApp.jpg) 

Sie sollten eine Login-Maske sehen. Oberhalb von der Login-Maske befinden sich die Sessiondaten, die der Server über Ihre Session gespeichert hat. Diese werden sich im Laufe Ihrer Aktivitäten verändern.

Gehen Sie wie folgt vor, um den Auftrag zu lösen:
1. Loggen Sie sich gemäss Anleitung auf der Seite ein. Beachten Sie dabei, wie sich die bei Ihnen gespeicherten Session-Daten verändern. Ihre Gruppenmitglieder sollen sich mit anderen Benutzerdaten Einloggen. Vergleichen Sie Ihre Session-Daten miteinander. Was fällt Ihnen auf? Sind diese Unterschiedlich oder gleich? Wo sind die Unterschiede? Hat sich jemand aus der Gruppe bereits als Admin eingeloggt? 
2. Geben Sie nun im eingeloggten Zustand eine Geheimbotschaft ein. Wie verändern sich die Sessiondaten?
3. Loggen Sie sich aus. Was verändert sich bei den Session-Daten?
4. Öffnen Sie nun die DEV-Tools des Browsers und untersuchen Sie die verwendeten Cookies und den Netzwerkverkehr. Wie ist Ihre Session-ID? Ändert sich diese nach dem Logout? Haben Sie eine Session-ID nur im eingeloggten Zustand oder ist diese auch vorher schon vorhanden?
5. Geben Sie nun Ihre Session-ID Ihren Gruppenmitgliedern bekannt. Diese sollen nun über Manipulation des Session-Cookies in ihren Browsern via DEV-Tools versuchen Ihre Session zu klauen. Was passiert mit den Session-Daten, wenn Sie bei sich die Session-ID im Cookie erfassen, die vorher bei einem anderen Gruppenmitglied erfasst war? Können Sie nun seine Session-Daten sehen? 
6. Was passiert, wenn sich das Gruppenmitglied ausloggt? Können Sie dann die Session-Daten immer noch sehen?
7. Challenge (optional): Analysieren Sie den Code der Webapplikation. Welche Sicherheitslücken enthält dieser? Wie könnten Sie diese fixen? Gäbe es allenfalls Wege das derzeit implementierte Sessionhandling noch sicherer zu machen?

Dokumentieren Sie Ihre Findings und Antworten in Ihrem Persönlichen Repository.