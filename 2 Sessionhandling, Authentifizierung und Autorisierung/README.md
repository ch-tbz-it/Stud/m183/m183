# Sessionhandling, Authentifizierung und Autorisierung
- Das Sessionhandling bildet die Grundlage, um in einer Applikation die Authentifizierung und Autorisierung durchzuführen.
[Weitere Details](./Sessionhandling)
- Bei der Authentifizierung (Überprüfung von Behauptungen) und Autorisierung (Berechtigungen erteilen) geht es darum, festzulegen, wer was tun darf.
[Weitere Details](./AuthentifizierungAutorisierung.md)