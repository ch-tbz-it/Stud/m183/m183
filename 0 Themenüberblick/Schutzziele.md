# Schutzziele
Schutzziele sind die Ziele, die durch die IT-Sicherheit erreicht werden sollen.

## Wichtige Schutzziele
Drei Schutzziele werden von allen Unternehmen / Branchen gleichermassen verfolgt. Diese lassen sich mit dem Kürzel CIA merken:
- **C**onfidentiality (Vertraulichkeit)
- **I**ntegrity (Integrität)
- **A**vailability (Verfügbarkeit)

Diese Schutzziele sind so allgemein gehalten, dass viele der weiteren Schutzziele (siehe weiter unten) durch die wichtigen Schutzziele abgedeckt sind. Für den Arbeitsalltag reicht es, wenn Sie mit den drei wichtigen Schutzzielen vertraut sind.

### Confidentiality (Vertraulichkeit)
Bei der Vertraulichkeit wird definiert, wer mit welchen Daten was tun darf. Um dieses Schutzziel zu erreichen, muss
- **Wer:** Damit das System weiss, von wem es bedient wird, muss sich der Benutzer erst *authentifizieren*. Der Benutzer muss - beispielsweise durch Angabe von Zugangsdaten - beweisen, dass er die Person ist, die auch im System hinterlegt wurde. Um einen Benutzer zu *authentifizieren* gibt es neben Benutzername und Passwort auch weitere Möglichkeiten die Sie im Kapitel [2 Authentifizierung und Autorisierung](../2%20Authentifizierung%20und%20Autorisierung) näher kennenlernen werden.
- **Welche Daten:** Rechte im Dateisystem werden üblicherweise auf Ordner oder File-Ebene vergeben. Normalerweise werden jedoch nicht einzelne Benutzer, sondern Gruppen von Benutzern berechtigt.
- **Was tun (Funktionalitäten):** Bei einem Dateisystem wird das "was tun dürfen" üblicherweise auf die Tätigkeiten lesen, schreiben, ausführen beschränkt. Gewisse Systeme erlauben weitere Aktionen zu spezifizieren wie beispielsweise löschen. Wenn es sich jedoch nicht um das Dateisystem als "Applikation" handelt, sondern beispielsweise um eine Webapplikation, die es Benutzern erlaubt Benutzerprofile zu bearbeiten, dann wird die Berechtigung sehr viel detaillierter ausfallen. Beispielsweise darf jeder Benutzer nur seine eigenen Profildaten bearbeiten - ausser beispielsweise Administratoren, die alle Profile bearbeiten dürfen. Bei Applikationen spielt oft auch der Kontext eine wichtige Rolle. So darf beispielsweise bei Magento selbst der Administrator nicht im Namen der Kunden Bestellungen aufgeben, ausser der Kunde setzt vorher in seinem Benutzeraccount den Haken, dass der Administrator das tun darf. Der Fachbegriff für die Definition davon "wer was tun darf" heisst *autorisieren* (Beispiel: Benutzer XY wird autorisiert auf die Ressource Z lesend zuzugreifen). Weitere Details über die Autorisierung werden Sie ebenfalls in Kapitel [2 Authentifizierung und Autorisierung](../2%20Authentifizierung%20und%20Autorisierung) näher kennenlernen.

Daten werden durch Verschlüsselung zusätzlich vor "nicht autorisierten" Zugriffen geschützt. Weitere Details dazu lernen Sie im Kapitel [3 Verschlüsselung](../3%20Verschlüsselung).

### Integrity (Integrität)
Bei der Integrität geht es darum, dass Informationen von unbefugten Modifikationen geschützt werden. Die Nachvollziehbar ist ein Hilfsmittel dies zu erreichen und zeichnet auf, was mit den Daten geschieht bzw. wann die Daten durch wen und allenfalls wie verändert wurden. Die Integrität und Nachvollziehbarkeit können wie folgt gewährleistet werden:

- **Wer**: Auch für dieses Schutzziel spielt die [Authentifizierung und Autorisierung](../2%20Authentifizierung%20und%20Autorisierung) eine wichtige Rolle. Informationen dürfen nur durch korrekt autorisierte Personen/System verändert werden. Es ist immer noch möglich, dass die Befugten die Integrität der Informationen verletzt, z.B. durch beabsichtigte oder unbeabsichtigte Fehlmodifikation. Hier spielt nun die Nachvollziehbarkeit eine Rolle mit den folgenden Punkten.
- **Logging und Monitoring:** Durch das Loggen von Veränderungen an Dateien oder auch Zugriffe auf Dateien wird nachvollziehbar, wer was mit den Daten gemacht hat. Bei Monitoring geht es darum, die Logfiles zu überwachen und bei wichtigen oder auch unerlaubten Aktivitäten zu alarmieren, sodass ein Administrator eingreifen und den Sicherheitsvorfall untersuchen und gegebenenfalls Schwachstellen schliessen kann. Im Kapitel [6 Logging und Monitoring](../6%20Logging%20und%20Monitoring) werden Sie alles Weitere dazu kennenlernen.
- **Backup:** Durch das Schreiben von Backups und Archiven und einer entsprechenden "Backup-History" wird es möglich durch Suchen in der History festzustellen was der Inhalt von File vor einiger Zeit war (beispielsweise vor einem Monat). Dadurch lässt sich nachvollziehen, was geändert hat (aber nicht unbedingt durch wen geändert wurde).

### Availability (Verfügbarkeit)
Bei der Verfügbarkeit geht es darum, dass die Systeme und Daten dann zur Verfügung stehen, wenn diese auch benötigt werden durch die Endbenutzer. Muss beispielsweise ein System für die Wartung für normale Benutzer gesperrt werden, so hat dies - um das Schutzziel der Verfügbarkeit zu wahren - ausserhalb der Bürozeiten zu erfolgen, weil dann niemand Daten vom System benötigt. 

Bei Daten werden Mechanismen implementiert, die vor unbeabsichtigtem Datenverlust schützen sollen. Folgende Massnahmen werden üblicherweise in Applikationen umgesetzt:
- **Meldung vor "destruktiven" Aktionen:** Bevor der User auf einer Applikation eine destruktive Aktion (beispielsweise unwiederrufliches löschen von Datensätzen) ausführen kann, wird ihm eine Meldung gezeigt, wo er bestätigen muss, dass er diese Aktion wirklich ausführen will.
- **Papierkorb:** häufig werden Datensätze nur als "gelöscht" markiert, aber noch in der Datenbank belassen, damit diese zu einem späteren Zeitpunkt bei Bedarf wiederhergestellt werden könnten. Üblich ist auch, dass als gelöscht markierte Daten nach einiger Zeit automatisch entfernt/bereinigt werden.
- **Backups:** Die Erstellung von Backups verhindert, dass gelöschte Daten oder defekte Daten unwiederbringlich weg sind. Indem regelmässig eine Kopie der Daten bzw. eine Kopie der Veränderungen an Daten vorgenommen wird, können gelöschte oder kaputte Daten (beispielsweise wenn die Daten durch Verschlüsselungstrojaner verschlüsselt wurden) wiederhergestellt werden. Im Kapitel [5 Backup](../5%20Backup) werden Sie alles Weitere dazu kennenlernen.

## Weitere Schutzziele
Es gibt einige weitere Schutzziele, die aber im Alltag weniger häufig verwendet werden:
- Authentizität (Authenticity)
- Nichtabstreitbarkeit (Non-Repudiation)
- Verlässlichkeit (Reliability)

Was sich hinter den weiteren Schutzzielen genau verbirgt erklärt der Artikel unter
https://www.michaelgorski.net/it-sicherheit-beratung/schutzziele-informationssicherheit/ sehr gut. Für den Arbeitsalltag sind jedoch vor allem die drei wichtigen Schutzziele (CIA) relevant.