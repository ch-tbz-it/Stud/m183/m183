# Applikationssicherheit implementieren
## Begriffe Applikations-, Netzwerk- und Host-Sicherheit
Im Modul 183 geht es um die Applikationssicherheit. Diese ist eine von drei "Sicherheitssäulen" im Bereich der IT-Security.
![AppNetHostSicherheit.png](AppNetHostSicherheit.png)

Dabei basiert die Applikationssicherheit auf der Netzwerksicherheit und der Host-Sicherheit. Wenn Fehler in der Netzwerk- oder Host-Sicherheit gemacht wurden, dann hilft es nicht, wenn Sie in der Applikationssicherheit alles richtig gemacht haben. Ihre Applikation wird dennoch angreifbar und verwundbar sein.

Stellen Sie sich vor, Sie haben eine Webapplikation, die aus Skripts besteht. Wenn jemand nun auf den Server unberechtigterweise Zugriff erhält, kann er Ihre Skripts nach Belieben abändern. Entsprechend ist - wegen der fehlenden Host-Sicherheit - auch die Applikationssicherheit in Gefahr. Dasselbe gilt für die Netzwerk-Sicherheit: wenn Sie in der Firewall gewisse Ports oder URLs nicht richtig geschützt haben, kann es sein, dass Angreifer auf interne Schnittstellen Ihrer Applikation Zugriff haben und so die Applikation "missbrauchen" können für Dinge, für die sie nicht gedacht ist.

Deshalb müssen wir in diesem Modul nicht ausschliesslich die Applikationssicherheit betrachten, sondern auch da und dort über den Gartenzaun hinaus schauen und gewisse "Exkurse" wagen. Der Fokus soll aber immer auf der Applikationssicherheit bleiben.

## Schutzziele
Schutzziele sind die Ziele die durch IT-Sicherheit erreicht werden sollen bzw. die Definition, was geschützt werden soll.

[Zu den Details](Schutzziele.md)

## Massnahmen
Um die Schutzziele zu erreichen sind Massnahmen nötig.

[Überblick über gängige Massnahmen](Massnahmen.md)

## Standards
Auch die Standards in der IT-Security geben Ziele vor und definieren dazu passende Massnahmen, um die Ziele zu erreichen. Auch im Zusammenhang mit Applikationssicherheit (aber auch mit Netzwerk- und Host-Sicherheit sowie der physischen Sicherheit) enthalten die Standards Ziele und Massnahmen.

[Überblick über Massnahmen in Standards, die mit der Applikationssicherheit zu tun haben](../7%20IT-Security%20Standards/README.md)