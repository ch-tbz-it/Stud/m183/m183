# Risikomanagement

[TOC]

## Einleitung
Bei Risikomanagement handelt es sich eher um eine Management-Disziplin. Applikationsentwickler haben häufig wenig mit Risikomanagement zu tun. Aber auch bei der Entwicklung und Verwendung von Applikationen können Risiken entstehen durch Schwachstellen in der Applikation oder durch die Fähigkeiten / Features der Applikation.

Risikomanagement beschäftigt sich mit dem Umgang der Risiken - wie diese kontrolliert bzw. gemanaged werden können. Das hat auch Einfluss auf die Art und Weise wie eine Applikation entwickelt wird - insbesondere dann, wenn es darum geht zu entscheiden, welche Schwachstellen in einer Applikation akzeptiert werden und welche gefixt werden müssen. Vor allem bei bestehenden Applikationen, die bereits produktiv genutzt werden, stellt sich diese Frage. Nach welchen Kriterien beurteilt wird, wann und welche Schwachstellen wie geschlossen werden sollen, damit beschäftigt sich dieses Kapitel.

## Identifizierung & Beschreibung von Risiken
Werden Sicherheitsmängel / Lücken entdeckt (durch Zufall oder bei systematischen Penetrationtests), ist die Abschätzung der folgenden Faktoren wichtig, um eine fundierte Entscheidungsbasis zu haben für das weitere Vorgehen:
* **Eintrittswahrscheinlichkeit:** Es wird abgeschätzt, wie wahrscheinlich es ist, dass ein Angreifer die Schwachstelle findet und auch versucht diese auszunutzen bzw. wie wahrscheinlich es ist, dass er diese auch tatsächlich ausnutzen kann.
* **Schadenspotenzial:** Es wird abgeschätzt wie gross der Schaden durch eine mögliche Ausnutzung der Schwachstelle maximal sein könnte. Dabei wird einerseits geschaut, welche materiellen Schäden (finanziell) und welche immateriellen Schäden (Imageverlust oder ähnliches, aber auch Bedrohung von Menschenleben) im schlimmstmöglichen Fall entstehen können.
* **Auswirkung:** Es wird zudem analysiert, ob der Angreifer durch die Schwachstelle nur auf das direkt betroffene System Zugriff hat oder ob er dadurch mehr Möglichkeiten hat andere Systeme anzugreifen. Es wird auch geschaut welche weiteren Systeme von einem Vorfall betroffen sein könnten.

In der Regel wird dann aufgrund von einer Risikomatrix entschieden, ob etwas getan werden muss, um entweder die Eintrittswahrscheinlichkeit zu minimieren oder das Schadenspotenzial zu minimieren bzw. die Auswirkungen einzugrenzen.

![Risikograph](Risikograph.png)
*Quelle Bild & weitere Details: https://de.wikipedia.org/wiki/Risikomanagement*

Welche Bereiche Rot, Gelb oder Grün markiert wird, entscheidet jedes Unternehmen für sich selbst. In diesem Zusammenhang wird auch von Risikobereitschaft gesprochen. Unternehmen die eine hohe Risikobereitschaft haben, werden den grünen Bereich grösser wählen. Unternehmen mit geringerer Risikobereitschaft werden den roten Bereich grösser wählen. Risiken die im grünen Bereich eingestuft werden (also Eintrittswahrscheinlichkeit gering und Auswirkung unwesentlich), die können als sogenanntes **Restrisiko** akzeptiert werden. Da müssen keine Massnahmen getroffen werden. Liegt das Risiko im gelben Bereich, dann muss dieses genauer überwacht und gegebenenfalls Massnahmen ergriffen werden. Bei Risiken im roten Bereich (treten häufig ein und der Schaden ist katastrophal hoch) müssen zwingend Massnahmen getroffen werden um das Risiko zu minimieren und in einen gelben oder grünen Bereich zu bringen.


## Planung von Massnahmen
Sind Massnahmen nötig, dann gibt es grundsätzlich zwei Möglichkeiten die Situation zu verbessern:
* **Schadensausmass reduzieren:** Die Auswirkungen eines Angriffs können durch Massnahmen minimiert werden oder der Schaden kann auf Dritte abgewälzt werden (beispielsweise durch Abschluss einer Cyberversicherung).
* **Eintrittswahrscheinlichkeit reduzieren:** Beispielsweise durch schliessen von Sicherheitslücken kann die Eintrittswahrscheinlichkeit im Idealfall auf 0% reduziert werden. Manchmal lässt sich die Eintrittswahrscheinlichkeit aber auch nur verringern (abhängig von der Art der Lücke und den vorhandenen technischen Möglichkeiten).

Generell gilt bei der Wahl der Massnahme: 

Wenn die Kosten der getroffenen Massnahmen geringer sind wie der Nutzen, der dadurch entsteht, wird die Unternehmensleitung in der Regel zugunsten von Optimierungsmassnahmen an der Applikation oder am System entscheiden. Wenn die Kosten für die Massnahmen jedoch höher sind, wie der erwartete maximale Schaden, dann wird in der Regel nichts unternommen - oder allenfalls der mögliche Schaden mit einer Versicherung abgedeckt, die im Fall eines erfolgreichen Angriffs dann den Schaden übernimmt.