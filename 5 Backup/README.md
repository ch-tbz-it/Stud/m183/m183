# Backup
Ein Backup ist eine Kopie von Daten die an einem anderen Ort wie das Original abgespeichert und aufbewahrt wird. Ziel von Backups ist es, die Originaldaten im Falle eines Verlusts wiederherstellen zu können. Die Verfügbarkeit (Availability) der Daten soll dadurch sichergestellt werden.

Im Zusammenhang mit Backups werden wir die folgenden wichtigen Fachbegriffe näher kennenlernen:
* **Archiv**: Das Ziel eines Datenarchivs ist es, die Daten unveränderbar und für einen längeren Zeitraum zu erhalten.
* **Online-Backup**: Ein Backup, dass auf Backup-Medien gespeichert wird, die jederzeit zugreifbar, auslesbar und auch beschreibbar sind.
* **Offline-Backup**: Nach dem Schreiben des Backups werden die Backup-Medien beim offline-Backup physisch aus dem Lese/Schreib-Gerät entfernt und offline (also ohne Verbindung zu irgendwelchen anderen Systemen) gelagert.

Die Details zu den Fachbegriffen werden in den folgenden Kapiteln ausgeführt.

## Unterschiedliche Arten von Backups
### Archiv != Backup
Der Hauptunterschied zwischen Backup und Archiv ist die Bestimmung bzw. der Zweck des Archivs. Ein Archiv dient vor allem dazu den im Gesetz festgesetzten Aufbewahrungsfristen gerecht zu werden oder aber historisch wichtige Daten zu archivieren, damit spätere Generationen auch noch Zugriff darauf haben. Eine Sicherung für die Archivierung kann auch auf unveränderlichen Medien wie beispielsweise DVD's stattfinden.

Wichtig bei Archiven ist, dass in Regelmässigen Abständen geprüft wird, ob die Daten noch lesbar sind. Ein Umspeichern von Archivdaten auf neuere Datenträger kann auch sinnvoll sein. Insbesondere ist es dann sinnvoll, wenn für ältere Speichermedien die Lese/Schreib-Geräte nicht mehr produziert werden oder wenn beispielsweise magnetische Speichermedien die Magnetisierung langsam verlieren. Um Archive zu Pflegen bzw. die Archivdaten zu erhalten ist folglich immer ein gewisser Aufwand nötig.

In der Regel werden Archive offline gelagert (d. h. ohne direkte Verbindung mit einem Lese/Schreibgerät) und sind daher relativ ähnlich zu den Offline-Backups - ausser dass das Ziel von Archiven ein anderes ist.

### Online-Backup
Ein Online-Backup hat zum Ziel vor Datenverlust (beispielsweise durch unabsichtliches Löschen) zu schützen und eine Möglichkeit der schnellen und einfachen Datenwiederherstellung zu bieten. Löscht ein Mitarbeiter beispielsweise unabsichtlich eine Datei, dann kann diese aus dem Backup wiederhergestellt werden.

**Wichtig:** Online-Backups müssen nicht unbedingt in der Cloud gespeichert werden, sondern können auch auf lokalen Systemen gespeichert werden. Ein Online-Backup ist dann eines, wenn die Speichermedien im Gerät verbleiben, dass die Backups schreibt und jederzeit ausgelesen und wiederhergestellt werden können.

Das Online-Backup bietet deshalb den Vorteil einer schnellen und einfachen Wiederherstellung, weil das Backup-Medium bereits im Lesegerät eingelegt ist und direkt ausgelesen werden kann (im Gegensatz zu offline-Backups wo die Datenträger erst ins Lesegerät eingelegt werden müssen). Ein Nachteil liegt aber darin, dass ein Verschlüsselungstrojaner auch den Backup-Server erwischen und die Backups verschlüsseln könnte. Damit wären die Backups nach einem Angriff mit Verschlüsselungstrojaner (z. B. einem [Ransomware-Angriff](https://de.wikipedia.org/wiki/Ransomware)) ebenfalls nicht mehr brauchbar. Das Unternehmen kann so alle Daten verlieren. Deshalb sollte ein Online-Backup immer auch noch durch ein Offline-Backup ergänzt werden.

### Offline-Backup
Offline-Backups sind Backups auf Backup-Medien, die physisch aus dem Backup-Gerät (da wo die Backups geschrieben werden) entfernt wird. Der grosse Vorteil dieser Backup-Art ist, dass sie gegen Ransomware-Angriffe geschützt ist, weil es auf rein digitalem Weg keine Möglichkeit gibt, die aus dem System entfernten Speichermedien zu überschreiben bzw. verschlüsseln. Der Nachteil ist jedoch, dass die Verwaltung der Speichermedien etwas aufwendiger ist wie bei den Online-Backups. Zudem dauert die Wiederherstellung in der Regel länger, weil die Speichermedien erst wieder in ein Lesegerät eingelegt werden müssen.

### Backup von Datenbanken
In Datenbanken werden in der Regel regelmässig ändernde Daten gespeichert. Bei Datenbanken haben Sie mit einem "DB-Dump" die Möglichkeit, einen konsistenten Zustand (Snapshot) im laufenden Betrieb der Datenbank zu sichern. Bei sehr intensiv genutzten Datenbanken kann das aber unter umständen zu wenig sein. Da haben Sie die Möglichkeit "binary logs" schreiben zu lassen (durch das Datenbanksystem). Diese binary logs zeichnen die SQL-Statements auf, die die Datenbasis verändern. Mit dem DB-Dump und den binary logs haben Sie nun die Möglichkeit, das Backup wieder einzuspielen und anschliessend die im binary log dokumentierten SQL-Statements durch das Datenbanksystem nochmals auf dem Backup ausführen zu lassen bis zu dem Zeitpunkt vor dem Vorfall. Damit können Sie den Datenverlust auf dein Minimum reduzieren (bzw. im Idealfall ganz verhindern).

Weil binary logs jedoch relativ schnell viel Speicherplatz einnehmen können, sind diese in der Regel in den Standardeinstellungen deaktiviert und müssen explizit aktiviert werden, um verwendet werden zu können.

## Generelle Sicherheitsaspekte bei Backups
Backups bündeln alle Daten, die für ein Unternehmen relevant sind. Darunter befinden sich auch sensible sowie personenbezogene Informationen. Entsprechend muss der Zugriff auf Backup-Medien klar geregelt und so gut wie möglich eingeschränkt werden.

Folgende Massnahmen helfen, die Backups sinnvoll zu schützen:
* **Verschlüsselung der Backups**: Werden die geschriebenen Backups verschlüsselt, dann bleiben die Daten im Falle eines Diebstahls gegen unbefugten Zugriff geschützt. Der Dieb/Angreifer müsste zusätzlich auch noch den Schlüssel für die Entschlüsselung der Daten kriegen, um mit den Backups etwas anfangen zu können. Wichtig hier: denken Sie daran, die Schlüssel sicher aufzubewahren - und nicht nur an einem Ort. Wenn Ihnen selbst der Schlüssel abhandenkommt, sind auch Ihre eigenen Backups für Sie nutzlos. Hier ist eine Abwägung zu treffen, welches Schutzziel (Confidentiality oder Availability) Ihnen wichtiger ist.
* **Sichere Lagerung der Backups**: Entscheiden Sie sich beispielsweise gegen eine Verschlüsselung der Backups um die Availability stärker zu gewichten, dann ist es entscheidend, dass die Backups sicher gelagert werden. Dies kann beispielsweise in einem Bankschliessfach oder im Tresor der Unternehmung sein.
* **Lagerort der Backups**: Bei der Lagerung müssen Sie darauf achten, dass die Backups an einem anderen Ort gelagert werden, wie die produktiven Daten. Werden die produktiven Daten und die Backups im gleichen Gebäude gelagert, können beispielsweise bei einem Brand sowohl die produktiven Daten wie auch die Backups betroffen sein.

Weiter ist wichtig, dass Sie auch in regelmässigen Abständen prüfen, ob Sie die Backups wiederherstellen können und ob auch alle Daten korrekt gespeichert wurden. Wird dies nicht gemacht, ist die Gefahr gross, dass Backups - wenn sie benötigt werden - aus diversen Gründen nicht wiederhergestellt werden können.