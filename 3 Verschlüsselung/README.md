M183 Applikationssicherheit implementieren

# Verschlüsselung / Kryptographie

<sup>Quellen: [hornetsecurity.com](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/), [ionos.de](ionos.de), [bsi.bund.de](https://www.bsi.bund.de), Wikipedia</sup>

Personenbezogene Daten müssen verschlüsselt werden, sobald diese über das Internet übermittelt oder in der Cloud gespeichert werden. Wurde noch vor einigen Jahren kaum an verschlüsselte Kommunikation gedacht, ist sie heute gefordert.

Um Daten die über Netzwerke transferiert werden gegen unbefugtes Mitlesen zu schützen, kommen diverse Verschlüsselungsalgorithmen zum Einsatz. 

'Verschlüsselung' ist jedoch keine Erfindung der Neuzeit. Historisch betrachtet liegen die Anfänge bereits sehr weit zurück, denn schon der römische Feldherr Gaius Julius Cäsar tauschte verschlüsselte Botschaften mit seinen Heerführern aus. Wir wollen in diesem Beitrag einen weiten Blick zurück in die Vergangenheit werfen, um die heutige 'Kryptographie' besser verstehen zu können.

## Cäsar-Chiffre

Die Cäsar-Chiffre ist ein einfaches symmetrisches Verschlüsselungsverfahren und basiert auf einer Substitution. Das bedeutet, dass jeder verwendete Buchstabe in der Nachricht durch einen neuen Buchstaben ersetzt wird. Der ersetzende Buchstabe ergibt sich aus einem Buchstabenversatz innerhalb des Alphabets, der im Voraus festgelegt wird, etwa eine Verschiebung um drei Stellen. Aus „Danke“ wird dann also „Gdqnh“. Für die Entschlüsselung wurde oft eine Chiffrierscheibe verwendet, um nicht dauernd das Alphabet vorbeten zu müssen. Dem Empfänger musste bei dieser Art der Verschlüsselung also lediglich der Versatz als geheimer Schlüssel im Voraus mitgeteilt werden.

Ein Unbefugter konnte ohne den Schlüssel zunächst nichts mit der Nachricht anfangen, doch hat er sich eine Zeit lang damit beschäftigt, ist es nach 25 Versuchen ein Leichtes die Nachricht zu entschlüsseln. Denn er musste maximal einmal das Alphabet prüfen, um den richtigen Buchstabenversatz aufzudecken. Heutige Computer würden dafür weniger als eine Sekunde benötigen.

Die Cäsar-Chiffre gilt heutzutage somit nicht mehr als sicher und wurde durch neuere Verfahren abgelöst.

https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung

## Vigenère Verschlüsselung

Eine der Methoden, die die Cäsar-Chiffre als sichere Alternative ablöste, war die des französischen Diplomaten und Kryptographen Blaise de Vigenère im 16. Jahrhundert, auch bekannt als Vigenère-Chiffre. Sie ist vergleichbar mit der Cäsar-Chiffre und basiert auch auf der Substitution von Buchstaben, jedoch werden dabei mehrere Geheimtextalphabete genutzt.

Wie viele Alphabete genutzt werden, wird durch einen Schlüssel bestimmt. Statt einer Zahl wird hier ein Schlüsselwort gewählt, welches unter die Botschaft geschrieben wird. Das Schlüsselwort bestimmt pro Buchstaben den Buchstabenversatz. Der erste Buchstabe des Schlüsselwortes bestimmt das Alphabet für den ersten Buchstaben des Klartextes, der zweite Buchstabe des Schlüsselwortes bestimmt das Alphabet für den zweiten Klartextbuchstaben.

```
Beispiel Vigenère Verschlüsselung

Schlüsselwort: GESCHENK (wird wiederholt aufgetragen)
Nachricht:     Wir schenken Tom einen Gutschein zum Geburtstag

G E S C H E N K G E S C H E N K G E S C H E N K G E S C H E N K G E S C H E N K G

W I R S C H E N K E N T O M E I N E N G U T S C H E I N Z U M G E B U R T S T A G

```

Zur Verschlüsselung brauchen wir ein !["Vigenère-Quadrat"](https://de.wikipedia.org/wiki/Vigen%C3%A8re-Chiffre#/media/Datei:Vigen%C3%A8re_square_shading.svg)

https://de.wikipedia.org/wiki/Vigen%C3%A8re-Chiffre

```
Das „G“ gibt nun einen Buchstabenversatz um sieben Buchstaben vor, da das „G“ an siebter Stelle im Alphabet steht. Das „E“ einen Versatz um fünf Buchstaben und so weiter. So wird aus dem „WIR“ ein „DNK“.
```

Die **Sicherheit**: dieses Verschlüsselungsverfahrens hängt stark mit der Schlüssellänge zusammen und damit, ob man den Schlüssel wiederholt verwendet. Wirklich sicher ist das Schlüsselwort aus unserem Beispiel daher nicht.

Doch nach einigen Jahren erwies sich auch dieses Verschlüsselungsverfahren als entzifferbar.

## Enigma 

Verschlüsselungsverfahren vorrangig im militärischen Kontext genutzt. Kaum verwunderlich daher, dass auch Deutschland während des Zweiten Weltkriegs auf verschlüsselte Kommunikation setzte. Das Besondere an dieser Verschlüsselung war, dass sie mithilfe einer Maschine ver- und entschlüsselt wurde (**Enigma**). Der Schlüssel dazu wurde jeden Tag geändert, sodass er nach 24 Stunden seine Gültigkeit verlor.

Diese Spezialmaschine zur routinemäßigen Chiffrierung und Dechiffrierung wurde von Arthur Scherbius 1918 erfunden. Das grundlegende Arbeitsprinzip geht bereits auf die Jahre des Ersten Weltkriegs zurück. Der Erste Weltkrieg gilt als der erste Krieg, in dem Kryptographie systematisch genutzt wurde. Bereits während des Krieges und auch in den Jahren danach wurden erste Maschinen entwickelt, die eine deutlich höhere Sicherheit boten als die manuellen Methoden. Enigma wurde nach ihrer Herstellung zum Kauf angeboten, stieß aber sowohl in der Wirtschaft als auch bei staatlichen Stellen auf sehr geringes Interesse. Erst 1933 gehörte die Enigma unter Hitler zur Standardausrüstung der Nationalsozialisten. 

Funktionsweise: *Auf den ersten Blick erinnert sie an eine klassische Schreibmaschine und beinhaltet einfache Stromkreise, von denen jeder eine Buchstabentaste des Tastenfelds mit einem elektrischen Lämpchen verbunden ist, das auf dem Anzeigenfeld einen Buchstaben aufleuchten lässt. Dabei ist das „A“ jedoch nicht mit dem „A“ des Anzeigenfelds verbunden. Alle Walzen sind nach einem bestimmten System miteinander verschränkt sodass die Nachricht nur entschlüsselt werden, wenn der Empfänger alle Einstellungen der Sende-Enigma kennt.*

Enigma wurde 1941 vom britischen Informatiker Alan Turing mit einer selbst entwickelten „Turing-Maschine“ geknackt. Historiker behaupten, dass so der Zweite Weltkrieg vorzeitig beendet werden konnte und Millionen Leben rettete.


## Symmetrische Verschlüsselung

Die Sicherheit eines (symmetrischen) Verschlüsselungsverfahrens beruht auf der Sicherheit des Schlüssels anstatt auf der Geheimhaltung des Algorithmus.

Das Prinzip der symmetrischen Verschlüsselung beschreibt die Ver- und Entschlüsselung zwischen Sender und Empfänger mit ein und demselben Schlüssel. Jedoch muss der Schlüssel gemeinsam mit der chiffrierten Nachricht zunächst an den Empfänger übergeben werden, damit dieser die Nachricht im Klartext lesen kann. Also etwa wie der Bote, der früher vom Sender mit der Botschaft zum Empfänger reiste, um ihm diese zu überbringen. Bis in die 1970er-Jahre wurden ausschließlich symmetrische Verschlüsselungsverfahren eingesetzt.

## Data Encryption Standard (DES)

DES wurde in den 1970er-Jahren entwickelt, nachdem die NSA eine Ausschreibung zur Entwicklung eines einheitlichen Standards für die behördenübergreifende Verschlüsselung vertraulicher Daten veröffentlichte.

Ein IBM-Entwicklungsteam um Walter Tuchman, Don Coppersmith und Alan Konheim reichte daraufhin einen vielversprechenden Vorschlag für ein entsprechendes Verschlüsselungsverfahren ein und wurde prompt beauftragt. Am 17. März 1975 wurde der von IBM entworfene Algorithmus im Federal Register veröffentlicht und bereits ein Jahr später als Verschlüsselungsstandard zugelassen.

DES nutzt einen 56-Bit-Schlüssel und eine Kombination aus Diffusions- und Konfusionselementen. Die zu verschlüsselnde Information wird in viele gleich große Blöcke zerteilt. Jeder Block wird unter Verwendung eines Runden-Schlüssels einzeln chiffriert und in 16 Runden, auch Iterationen genannt, „verwürfelt“. Um die Botschaft wieder zu entschlüsseln, müssen die Blöcke wieder in die richtige Reihenfolge gebracht werden.

Vielfach kamen Fragen zu der Rolle der NSA in diesem Projekt auf. Die NSA soll bewusst eine Hintertür eingebaut haben, um so die verschlüsselten Informationen mitlesen zu können. Anlass zu den Vermutungen gaben vor allem die Diskussionen um die Schlüssellänge: IBM bevorzugte eine Schlüssellänge von 64 Bits während die NSA eine Schlüssellänge von 48 Bits als ausreichend ansah. Geeinigt wurde sich dann auf 56 Bits.

Weitreichend eingesetzt wurde DES bei Geldautomaten, es wurde daher als ein sehr sicheres Verschlüsselungssystem angesehen. 1998 gelang es jedoch mit „Deep Crack“ erstmalig den 56-Bit-Schlüssel zu knacken. Das Gerät konnte den DES-Algorithmus innerhalb weniger Tage per Brute-Force-Methode entschlüsseln. Heutzutage wäre dies bereits in kürzester Zeit möglich. DES wurde daher eine hohe Anfälligkeit für Brute-Force-Attacken attestiert.

2001 löste der Advanced Encryption Standard AES den DES als Nachfolger ab. 

https://de.wikipedia.org/wiki/Data_Encryption_Standard


## Advanced Encryption Standard (AES)

Da DES seit den 1990er-Jahren mit seinem 56-Bit-Schlüssel nicht mehr ausreichend gegen Brute-Force-Angriffe gesichert war, schrieb das amerikanische Handelsministerium 1997 die Suche nach einem Nachfolgealgorithmus aus. Um eine gewisse Sicherheit des Advanced Encryption Standards zu gewährleisten, musste der Algorithmus bestimmte Kriterien erfüllen.

Auswahlkriterien AES:

• Symmetrischer Algorithmus, Blockchiffre

• Verwendung von 128 Bit langen Blöcken

• Einsetzen von 128, 192 und 256 Bit langen Schlüsseln möglich

• Überdurchschnittliche Leistung in Hardware und Software

• Widerstandsfähigkeit gegen Kryptoanalyse

Es wurden fünfzehn Vorschläge eingereicht. Zu den fünf besten Kandidaten zählten die Algorithmen MARS, RC6, Rijndael, Serpent und Twofish. Da alle Kandidaten die geforderten Kriterien erfüllten, wurden weitere Anforderungen aufgestellt. Sieger wurde im Jahr 2000 der belgische Rijndael, vor allem durch seine einfache Softwareimplementierung, Sicherheit und Geschwindigkeit.

Der Einsatz von AES-Verschlüsselung ist weitverbreitet, etwa bei Wireless LAN, VPNs, VoIP-Telefonie oder der Verschlüsselung von Dateien.

https://de.wikipedia.org/wiki/Advanced_Encryption_Standard

## Asymmetrische Verschlüsselung

Auch der Nachfolger von DES, der AES benutzt die 'symmetrische Verschlüsselung'. 

Zur asymmetrischen Verschlüsselung braucht man zwei Schlüsselpaare. **Private Key** und **Public Key**.

Der Sender verschlüsselt seine Botschaft mit dem Public Key des Empfängers. Der Empfänger kann die Botschaft dann wiederum nur mit seinem Private Key entschlüsseln. Der Private Key ist __privat__ und bleibt auf den eigenen Geräten gespeichert.

## S/MIME

Das S/MIME (Secure/Multipurpose Internet Mail Extensions) Protokoll nutzt eine hierarchische Public Key Infrastructure (PKI). Es geht also um die **Verschlüsselung des Mailverkehrs**.

Im Zusammenhang mit S/MIME wird meistens von **Zertifikaten** statt Schlüsseln gesprochen. Den Vertrauensanker bildet dabei die Root Certificate Authority (Root CA), die das Root Certificate sicher verwahrt. Mit diesem sind Zertifikate der untergeordneten Certificate Autorities (CA) beglaubigt. Diese wiederum stellen die Zertifikate für die Endnutzerinnen und -nutzer aus und beglaubigen diese. Die Gültigkeit und Authentizität eines Endnutzerzertifikates kann durch das Prüfen der Zertifikatskette bis zum Root Certificate verifiziert werden.

Die benötigten Schlüsselpaare bzw. Zertifikate werden dabei üblicherweise nicht von den Nutzerinnen und Nutzern selbst erzeugt und verteilt, sondern von Organisationen oder Firmen, und sind zumeist kostenpflichtig. Daher eignet sich S/MIME vor allem für Unternehmen oder Behörden und weniger für den privaten Gebrauch. 

https://www.ionos.de/digitalguide/e-mail/e-mail-sicherheit/smime-das-standardverfahren-fuer-mail-verschluesselung/

https://de.wikipedia.org/wiki/S/MIME


## (Open)PGP 

Bei OpenPGP ist das Vertrauensmodell für die Schlüssel viel flacher organisiert, oft auch in einem sogenannten Netz des Vertrauens (Web of Trust), bei dem indirekt einem fremden Schlüssel vertraut wird, der von jemandem signiert wurde, dem vertraut wird. Die Schlüssel werden von Nutzerinnen und Nutzern selbst erstellt und verwaltet.

Bei der Bewertung des Vertrauens in die Schlüssel der Kommunikationspartner helfen einige E-Mail-Tools. Dabei hängt das Vertrauen oft von der Quelle oder dem Weg ab, über den der Schlüssel bezogen wurde. Zum Beispiel kann ein Schlüssel per E-Mail, Schlüsselserver (der die Authentizität prüft oder auch nicht), Webseite oder auch persönlich auf einem USB-Stick erhalten werden. In jedem Fall kann die Authentizität des Schlüssels überprüft werden, indem der eindeutige Fingerabdruck des Schlüssels über einen anderen Kanal abgeglichen wird (z. B. Telefon oder Messenger) als der, über den der Schlüssel erhalten wurde.

https://de.wikipedia.org/wiki/Pretty_Good_Privacy

## RSA

Das RSA-Kryptosystem [18:57 min, D, YouTube, Weiz, 2018](https://www.youtube.com/watch?v=mDRMzBlI3U4) 

RSA - Verschlüsselung
47 min, 5 Videos in YouTube Playlist, Vorlesung Prof. Chr. Spannagel

https://www.youtube.com/playlist?list=PLdVk34QLniSBox_E5IFU9S4zSszFE2RsJ&disable_polymer=true

RSA Verschlüsselung mit Schlüsselgenerierung und Modelldurchlauf- it-archiv.net [8:50 min, D, YouTube](https://www.youtube.com/watch?v=gFd_SZZF63I)



## Digital signieren

https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/C.%20Grundlagen%20Kryptografie/C.3%20Digital%20signieren

## Schlüsselaustauschverfahren

Das Problem ist, dass ich bei der Übermittlung einer Nachricht eigentlich auch den Schlüssel zur Nachricht mitgeben muss. Es handelt sich hier um eine symmetrische Verschlüsselung, das heisst, dass zum verschlüsseln und zum entschlüsseln, also auf beiden Seiten, der gleiche Schlüssel verwendet wird.
Aber wenn ich den Schlüssel der Nachricht mitgebe, dann kann jeder auch die verschlüsselte Nachricht entschlüsseln und lesen. Der Schlüssel muss also "geheim" übermittelt werden.

So müsste ich den Schlüssel vor oder nach dem Verschicken der Nachricht dem Empfänger bekanntgeben. Wenn ich aber den Schlüssel auf einem anderen Weg dem Empfänger geben muss, dann wird das auch schwierig.

### Diffie-Hellman, Merkle - Schlüsselaustauschverfahren

- [Diffie-Hellman-Schlüsselaustauschverfahren, 6:01 min, D](https://www.youtube.com/watch?v=_E0SGl7aN70) anschaulich auf Whiteboard erklärt.
- [Secret Key Exchange, 8:39 min, E](https://www.youtube.com/watch?v=NmM9HA2MQGI) anschaulich erklärt auf Papier und mit Flüssigkeiten 
- [Diffie-Hellmann-Mathematics, 7.04 min, E](https://www.youtube.com/watch?v=Yjrfm_oRO0w)

<br>
<hr>

## Hash-Funktionen

Mit einer Hash-Funktion "zerhackt" (engl. hash) eine Eingabemenge mit mathematischen Methoden auf eine Zielmenge so, dass möglichst für unterschiedliche unterschiedliche Eingaben auch zu unterschiedlichen Ausgabewerten führen. Aus dem resultierenden "Hashwert" kann man im allgemeinen nicht auf den Eingabewert schliessen (ist "injektiv" - nicht rückführbar).

Hash-Funktionen werden in Datenbanken angewandt um die Performance zu verbessern oder bei der Übertragung von Daten über die [Prüfsumme,checksum](https://de.wikipedia.org/wiki/Pr%C3%BCfsumme) die Echtheit zu garantieren und sie werden auch für [Kompressionsverfahren](https://de.wikipedia.org/wiki/Kompressionsalgorithmus) wie [LZW](https://de.wikipedia.org/wiki/LZW) für Filme, Bilder und Texte verwendet.

So werden Passwörter in Systemen eigentlich nie im Klartext sondern mit dem Hashwert abgespeichert.

Wichtige Hash-Funktionen sind [SHA](https://de.wikipedia.org/wiki/Secure_Hash_Algorithm), [MD2](https://de.wikipedia.org/wiki/MD2), [MD4](https://de.wikipedia.org/wiki/MD4), [MD5](https://de.wikipedia.org/wiki/MD5), [Mittquardratmethode](https://de.wikipedia.org/wiki/Mittquadratmethode) oder die [Multiplikative-](https://de.wikipedia.org/wiki/Multiplikative_Methode) und die [Divisions-](https://de.wikipedia.org/wiki/Divisionsrestmethode)Methode.

In jeder gebräuchlichen Programmiersprache ist SHA und MD5 für die einfache Benutzung eingebaut.


https://de.wikipedia.org/wiki/Hashfunktion

https://de.wikipedia.org/wiki/Kryptographische_Hashfunktion
